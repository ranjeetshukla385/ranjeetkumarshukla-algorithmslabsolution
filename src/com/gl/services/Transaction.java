package com.gl.services;

public class Transaction {
	// This function iterate the array and compare with target and return the no of
	// transaction take to achive the target
	public int verifyTransaction(int[] transArray, int target) {
		int count = 0;
		int noOfTrasaction = 0;
		for (int i = 0; i < transArray.length; i++) {
			count = count + transArray[i];
			if (target <= count) {
				noOfTrasaction = i + 1;
				break;
			}
		}
		return noOfTrasaction;
	}
}
