package com.gl.controller;

import java.util.Scanner;

import com.gl.services.Transaction;

public class PayMoney {
	public static void main(String[] args) {
		int transactionArraySize = 0;
		int totalTarget = 0;
		int noOfTargetToCheck = 0;
		int[] transactionArray;

		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter the size of transaction array");
		transactionArraySize = scanner.nextInt();

		// initialize array with size
		transactionArray = new int[transactionArraySize];

		// Taking the value from user in array
		System.out.println("Enter the values of array");
		for (int i = 0; i < transactionArraySize; i++) {
			transactionArray[i] = scanner.nextInt();
		}

		System.out.println("Enter the total no of targets that needs to be achieved");
		noOfTargetToCheck = scanner.nextInt();

		Transaction transaction = new Transaction();
		int noOfItration = 0;
		while (noOfItration < noOfTargetToCheck) {
			System.out.println("Enter the value of target");
			totalTarget = scanner.nextInt();

			int noOfTrasaction = transaction.verifyTransaction(transactionArray, totalTarget);
			String output = noOfTrasaction == 0 ? "Given target is not achieved"
					: "Target achieved after " + noOfTrasaction + " transactions";
			System.out.println(output);
			noOfItration++;
		}
		scanner.close();
	}
}
